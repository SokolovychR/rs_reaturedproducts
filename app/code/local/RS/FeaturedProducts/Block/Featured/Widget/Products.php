<?php

class RS_FeaturedProducts_Block_Featured_Widget_Products extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

    public function __construct (array $args = array())
    {
        parent::__construct($args);
        $this->setTemplate('featured_products/featured_widget_products.phtml');
    }

    public function getProductsCollection ()
    {
        $productsCollection = Mage::getModel('catalog/product')->getCollection();
        $productsCollection->addAttributeToFilter('use_in_featured_widget', 1);
        $selectAttributes = array( 'name', 'product_url' );
        if ($this->displayImages()) {
            $selectAttributes[] = 'small_image';
        }
        if ($this->displayPrices()) {
            $selectAttributes[] = 'price';
        }
        $productsCollection->addAttributeToSelect($selectAttributes);
        $productsCollection->setPageSize($this->getData('page_size'));
        return $productsCollection;
    }

    public function displayImages ()
    {
        return $this->getData('display_images');
    }

    public function displayPrices ()
    {
        return $this->getData('display_prices');
    }

    public function formatPrice ($product)
    {
        return Mage::helper('core')->currency($product->getPrice());
    }

    public function getAddToCartUrl($product, $additional = array())
    {
        if (!$product->getTypeInstance(true)->hasRequiredOptions($product)) {
            return $this->helper('checkout/cart')->getAddUrl($product, $additional);
        }
        $additional = array_merge(
            $additional,
            array(Mage_Core_Model_Url::FORM_KEY => $this->_getSingletonModel('core/session')->getFormKey())
        );
        if (!isset($additional['_escape'])) {
            $additional['_escape'] = true;
        }
        if (!isset($additional['_query'])) {
            $additional['_query'] = array();
        }
        $additional['_query']['options'] = 'cart';
        return $this->getProductUrl($product, $additional);
    }


}